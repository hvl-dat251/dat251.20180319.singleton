package dat251.singleton.solution3;

import java.util.Random;

public class Appthread extends Thread {
	
	@Override
	public void run() {
		Singleton si = Singleton.INSTANCE;
		
		while (si.doRun) {
			
			si.CallMe(this);
			
			Random x = new Random();
			int time = x.nextInt(2000);
			
			try {
				Thread.sleep(time);
			} catch (InterruptedException ie) {}
			
		}
	}

}

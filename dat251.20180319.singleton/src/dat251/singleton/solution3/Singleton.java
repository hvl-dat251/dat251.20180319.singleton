package dat251.singleton.solution3;

public enum Singleton {
	INSTANCE;
	
	
	private int callCount = 0;
	public boolean doRun = true;

	
	public void CallMe(Thread t) {
		callCount++;
		System.out.println("I have been called " + callCount + " times! [" + this.toString() + ";" + t.toString() + "]");
	}
	

}

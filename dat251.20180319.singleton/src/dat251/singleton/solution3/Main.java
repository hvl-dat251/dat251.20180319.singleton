package dat251.singleton.solution3;

public class Main {
	
	public static void main(String[] args) {
		
		for(int i = 0; i < 10; i++){
			Appthread a = new Appthread();
			a.start();
		}
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {}
		
		Singleton si = Singleton.INSTANCE;
		si.doRun = false; // stops all threads
		
	}
	
	

}

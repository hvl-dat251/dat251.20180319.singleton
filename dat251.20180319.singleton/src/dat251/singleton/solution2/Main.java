package dat251.singleton.solution2;

// Note that this example have intentional issues
// It is intended for analysis, not as an example
// of best practices.



public class Main {
	
	public static void main(String[] args) {
		
		for(int i = 0; i < 10; i++){
			Appthread a = new Appthread();
			a.start();
		}
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {}
		
		Singleton si = Singleton.Instance();
		si.doRun = false; // stops all threads
		
	}
	
	

}

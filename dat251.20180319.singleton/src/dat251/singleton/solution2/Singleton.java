package dat251.singleton.solution2;

public class Singleton {
	
	private static Singleton s;
	
	private int callCount = 0;
	public boolean doRun = true;
	
	private Singleton() {
		
	}
	
	public static Singleton Instance() {
		if(s == null) {
			s = new Singleton();
		}
		return s;
		//return new Singleton();
	}
	
	public void CallMe(Thread t) {
		callCount++;
		System.out.println("I have been called " + callCount + " times! [" + this.toString() + ";" + t.toString() + "]");
	}
	

}

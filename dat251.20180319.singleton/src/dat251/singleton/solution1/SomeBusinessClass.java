package dat251.singleton.solution1;

public class SomeBusinessClass {


	public void doWork() {

		DatabaseConnection dbc = DatabaseConnection.Instance();

		Human h = dbc.searchByName("Jane");

		System.out.println("Processing: " + h.getName() + ":" + h.getAge());


	}

}

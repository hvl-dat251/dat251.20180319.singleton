package dat251.singleton.solution1;

public class AnotherBusinessClass {

	public void analyzeEmployee(String name) {
		
		DatabaseConnection dbc = DatabaseConnection.Instance();
		
		Human h = dbc.searchByName(name);
		
		System.out.println("Analying the employee " + h.getName() + " (" + h.getAge() + ")");
		
	}
	
	
	
}

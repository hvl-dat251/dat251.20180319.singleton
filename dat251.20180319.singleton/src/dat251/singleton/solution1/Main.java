package dat251.singleton.solution1;

// Note that this example have intentional issues
// It is intended for analysis, not as an example
// of best practices.


public class Main {

	public static void main(String[] args) {
		
		SomeBusinessClass a = new SomeBusinessClass();
		AnotherBusinessClass b = new AnotherBusinessClass();
		
		a.doWork();
		b.analyzeEmployee("Elvis");
		
		
		

	}

}

package dat251.singleton.solution1;

import java.sql.Connection;

public class DatabaseConnection {
	
	private static DatabaseConnection instance;
	
	private Connection conn;
	
	
	private DatabaseConnection() {
		// Sets up a database connection here
		// Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" );
        // conn = DriverManager.getConnection("jdbc:odbc:animalsurvey", null, null );
		
		// For this example, we are only going to use some mock data instead of a
		// database though
		
		
	}
	
	public static DatabaseConnection Instance() {
		if (instance == null) {
			
			synchronized(DatabaseConnection.class) {
				if (instance == null) {
					instance = new DatabaseConnection();
				}
			}
		}
		
		return instance;
	}
	
	
	public Human searchByName(String name) {

		// for the sake of the example, we just create a new
		// instance instead of doing a DB lookup. In a real
		// program, we would have used the database connection
		// we (didn't) set up in the constructor.
		
		Human h = new Human();
		h.setName(name);
		h.setAge((int)(Math.random()*80));

		return h;
	}
		
		
		
}
	
	
